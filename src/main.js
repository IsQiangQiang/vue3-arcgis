import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import axios  from "axios";
import ElementPlus from 'element-plus'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import 'element-plus/dist/index.css'

const app = createApp(App)
app.config.globalProperties.axios = axios
app.use(router)
app.use(ElementPlus, {locale: zhCn,})
app.mount('#app')
